package controllers

import (
	"encoding/json"
	"fmt"

	"github.com/beego/beego/v2/core/logs"

	"github.com/opensourceways/app-cla-server/models"
	"github.com/opensourceways/app-cla-server/signing/domain/emailservice"
	"github.com/opensourceways/app-cla-server/signing/infrastructure/emailtmpl"
	"github.com/opensourceways/app-cla-server/worker"
)

const (
	headerToken                    = "Token"
	headerPasswordRetrievalKey     = "Password-Retrieval-Key"
	apiAccessController            = "access_controller"
	fileNameOfUploadingOrgSignatue = "org_signature_file"
)

func sendEmailToIndividual(to string, orgInfo *models.OrgInfo, subject string, builder emailservice.IEmailMessageBulder) {
	sendEmail([]string{to}, orgInfo, subject, builder)
}

func sendEmail(to []string, orgInfo *models.OrgInfo, subject string, builder emailservice.IEmailMessageBulder) {
	msg, err := builder.GenEmailMsg()
	if err != nil {
		logs.Error(err)

		return
	}
	msg.From = orgInfo.OrgEmail
	msg.To = to
	msg.Subject = subject

	worker.GetEmailWorker().SendSimpleMessage(orgInfo.OrgEmailPlatform, &msg)
}

func (ctl *baseController) notifyCorpAdmin(linkId string, orgInfo *models.OrgInfo, info *models.CorporationManagerCreateOption) {
	ctl.notifyCorpManagerWhenAdding(linkId, orgInfo, []models.CorporationManagerCreateOption{*info})
}

func (ctl *baseController) notifyCorpManagerWhenAdding(linkId string, orgInfo *models.OrgInfo, info []models.CorporationManagerCreateOption) {
	admin := info[0].Role == models.RoleAdmin
	subject := fmt.Sprintf(emailtmpl.SubjectTmpl.AddCorpAccount, orgInfo.OrgAlias)

	var signURL string
	if ctl.Ctx.Input.Domain() == config.CookieDomain {
		signURL = config.signingURL(linkId)
	} else {
		signURL = fmt.Sprintf("https://%s", ctl.Ctx.Request.Host)
	}

	for i := range info {
		item := &info[i]
		d := emailtmpl.AddingCorpManager{
			Admin:            admin,
			ID:               item.ID,
			User:             item.Name,
			Email:            item.Email,
			Password:         item.Password,
			Org:              orgInfo.OrgAlias,
			ProjectURL:       orgInfo.ProjectURL(),
			URLOfCLAPlatform: signURL,
		}

		sendEmailToIndividual(item.Email, orgInfo, subject, &d)

		// clear password
		for i := range item.Password {
			item.Password[i] = 0
		}
	}
}

func fetchInputPayloadData(input []byte, info interface{}) *failedApiResult {
	if err := json.Unmarshal(input, info); err != nil {
		return newFailedApiResult(
			400, errParsingApiBody, fmt.Errorf("invalid input payload: %s", err.Error()),
		)
	}
	return nil
}

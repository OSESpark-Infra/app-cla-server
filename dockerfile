# syntax=docker/dockerfile:latest

ARG GO_VERSION=latest
ARG GO_IMAGE=golang:${GO_VERSION}

FROM cgr.dev/chainguard/python:latest-dev AS build-python

WORKDIR /home/nonroot

RUN <<'EOF'
#!/bin/bash -xeu
python -m venv .venv
.venv/bin/pip install --upgrade pip setuptools wheel
.venv/bin/pip install git+https://github.com/py-pdf/pypdf.git@3.12.0
EOF

FROM ${GO_IMAGE} AS build-go

WORKDIR /go/src/cla-server
COPY . .

RUN --mount=type=cache,target=/go/pkg/mod <<'EOF'
#!/bin/bash -xeu
export GOPROXY=https://goproxy.io,direct
go build -o /go/bin/cla-server -buildmode=pie --ldflags '-s -linkmode "external" -extldflags "-Wl,-z,now"'
EOF

FROM cgr.dev/chainguard/python:latest AS final

COPY --link --chown=65532 --from=build-python /home/nonroot/.venv /home/nonroot/.venv
COPY --link --chown=65532 --from=build-go /go/bin/cla-server /home/nonroot/
COPY --link --chown=65532 conf /home/nonroot/conf
COPY --link --chown=65532 deploy/app.conf /home/nonroot/conf/
COPY --link --chown=65532 util/merge_signature.py /home/nonroot/util/merge_signature.py

WORKDIR /home/nonroot
ENTRYPOINT ["/home/nonroot/cla-server"]

package app

type ManagerDTO struct {
	Role      string
	Name      string
	Account   string
	Password  []byte
	EmailAddr string
}
